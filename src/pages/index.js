export { HomePage } from './Home/HomePage';
export { Faq } from './Home/components/Faq';
export { Hero } from './Home/components/Hero';
export { Testimonial } from './Home/components/Testimonial';
export { SliderCard } from './Home/components/SliderCard';
export { Accordion } from './Home/components/Accordion';
export { FeaturedProducts } from './Home/components/FeaturedProducts';
export { AboutUs } from './AboutUs'
export { FilterBar } from './product/components/FilterBar';
// export { ProductPage } from '../product/ProductPage';
export {ProductPage } from './product/ProductPage'
export { ProductDetailsPage } from './product/ProductDetailsPage';
export { Register } from './Auth/Register';
export { Login } from './Auth/Login';