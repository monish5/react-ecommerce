import React from "react";
import { Accordion } from "./Accordion";

const faqs = [
  {
    id: 1,
    question: "Are Plants Good For Health?",
    answer:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus earum dicta nesciunt, nulla alias consequuntur cumque incidunt saepe mollitia esse! Magni praesentium delectus excepturi nostrum illo repellendus cum eius neque, aperiam dolores quaerat quis dolore magnam doloremque minus sint nemo qui necessitatibus at. Perspiciatis, corrupti cum labore quos odio porro!",
  },
  {
    id: 2,
    question: "Are Plants Safe?",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. At accusamus nobis tempore perferendis qui, quam, atque reprehenderit vero quaerat, assumenda pariatur eveniet. Maxime eaque, neque corrupti ad minus repudiandae consectetur!",
  },
  {
    id: 3,
    question: "Can Plants Grow In Artificial Light?",
    answer:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse iste dolor deserunt expedita quam fugit et inventore amet pariatur. Animi.",
  },
  {
    id: 4,
    question: "What Types Of Delivery Options Are Available For Plants?",
    answer:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse iste dolor deserunt expedita quam fugit et inventore amet pariatur. Animi.",
  },
];

export const Faq = () => {
  return (
    <div className="container mx-auto dark:text-white mb-16" data-accordion="open">
      <h2 className='text-4xl font-medium mb-4 text-center sectionTitle'>FAQs</h2>
      { faqs.map(faq => <Accordion key={faq.id} faq={faq} />) }
      
    </div>
  );
};
