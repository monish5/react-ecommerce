import React from 'react'
import Slider from 'react-slick';
import { SliderCard } from './SliderCard';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const testimonials = [
  {
    id: 1,
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tristique.",
    author: "John Doe",
    company: "ABC Inc.",
  },
  {
    id: 2,
    text: "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    author: "Jane Smith",
    company: "XYZ Co.",
  },
  // Add more testimonials as needed
];

export const Testimonial = () => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <div className="container mx-auto dark:text-white mb-16">
      <h2 className='text-4xl font-medium mb-4 text-center sectionTitle'>Pure Reviews</h2>
      <Slider {...settings}>
        {testimonials.map(testimonial => <SliderCard key={testimonial.id} testimonial={testimonial} />)}
      </Slider>
    </div>
  )
}
