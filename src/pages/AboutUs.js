import React from 'react'

export const AboutUs = () => {
  return (
    <div>
        **Welcome to [Your E-commerce Site Name] - Where Nature Meets Elegance!**

        At [Your E-commerce Site Name], we believe that every space deserves a touch of green, a breath of fresh air, and a connection to the beauty of nature. Our journey began with a simple yet profound idea: to bring the serenity of the outdoors to your doorstep, making it easy for you to transform any space into a lush haven.

        **Our Passion for Plants:**
        Founded [Your E-commerce Site Name] with a deep passion for plants and a commitment to enhancing the lives of our customers. We understand the joy and tranquility that greenery brings, and we're here to share that joy with you. Whether you're a seasoned plant enthusiast or just beginning your journey into the world of gardening, we're your trusted partner in cultivating a vibrant and thriving environment.

        **Quality and Sustainability:**
        We take pride in offering a curated selection of the finest plants, each carefully chosen for its unique beauty and resilience. Our commitment to sustainability goes beyond our products – we source responsibly, minimize waste, and prioritize eco-friendly packaging to ensure a positive impact on the planet.

        **Expertise You Can Trust:**
        Backed by a team of passionate horticulturists and plant enthusiasts, we provide expert guidance and support to help you choose the perfect plants for your space. From low-maintenance succulents to statement-making tropicals, we're here to assist you at every step of your plant journey.

        **Customer-Centric Approach:**
        At [Your E-commerce Site Name], your satisfaction is our top priority. We strive to create an enjoyable shopping experience, offering convenient online browsing, secure transactions, and prompt delivery. Have a question or need advice? Our friendly customer support team is always ready to assist you.

        **Join Our Growing Community:**
        Beyond being an e-commerce platform, [Your E-commerce Site Name] is a community of plant lovers. Follow us on social media for inspiration, tips, and a chance to connect with like-minded individuals who share a love for all things green.

        Thank you for choosing [Your E-commerce Site Name] as your go-to destination for quality plants and unparalleled service. We look forward to being a part of your plant-filled journey, adding a touch of nature's magic to your life.

        Happy planting!
        The [Your E-commerce Site Name] Team
    </div>
  )
}
