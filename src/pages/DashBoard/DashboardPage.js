import { toast } from "react-toastify";
import { DashboardCard } from "./components/DashboardCard";
import { DashboardEmpty } from "./components/DashboardEmpty"
import { useEffect, useState } from "react";
import { getOrders } from "../../services/orderService";
import { useDynamicTitle } from "../../hooks";

export const DashboardPage = () => {
  const [orders, setOrders] = useState([]);
  useDynamicTitle("Dashboard | Purity Plants");
  useEffect(() => {
    async function fetchOrders() {
      try {
        const data = await getOrders();
        setOrders(data);
      } catch(error) {
        console.log(error)
        toast.error(error.message, {
          position: toast.POSITION.BOTTOM_RIGHT
        });
      }
    }
    fetchOrders();
  }, []);

  return (
    <main>
      <section>
        <p className="text-2xl text-center font-semibold dark:text-slate-100 my-10 underline underline-offset-8">My Dashboard</p>
      </section>

      <section>
        { orders.length && orders.map((order) => (
          <DashboardCard key={order.id} order={order} />
        )) }
      </section>

      <section>
        { !orders.length && <DashboardEmpty /> }
      </section>

    </main>
  )
}
