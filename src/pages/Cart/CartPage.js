import { useSelector } from "react-redux";
import { CartEmpty } from "./components/CartEmpty";
import { CartList } from "./components/CartList";
import { useDynamicTitle } from "../../hooks";

export const CartPage = () => {
    const { cartList } = useSelector( state => state.cart );
  useDynamicTitle('My Cart | Purity Plants');
    console.log("going in cart");
  return (
    <main>       
      { cartList.length ? <CartList /> : <CartEmpty /> }   
    </main>
  )
}
