import React from 'react'
import { getSessionData } from '../services/authService';
import { Navigate } from 'react-router-dom';

export const ProtectedRoute = ({ children, path, element }) => {
  const browserData = getSessionData();
  return browserData.token ?  children  : <Navigate to='/login' />
}
