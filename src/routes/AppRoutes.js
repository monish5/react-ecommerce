import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { AboutUs, HomePage, Login, ProductDetailsPage, ProductPage, Register } from '../pages'
import { CartPage } from '../pages/Cart/CartPage'
import { DashboardPage } from '../pages/DashBoard/DashboardPage'
import { getSessionData } from '../services/authService'
import { ProtectedRoute } from './ProtectedRoute'


export const AppRoutes = () => {
  return (
    <>
      <Routes>
        <Route path='/' element={ <HomePage /> } />
        <Route path='/aboutus' element={ < AboutUs />} />
        <Route path='/products' element={ <ProductPage /> } />
        <Route path='/products/:id' element={<ProductDetailsPage />} />
        <Route path='/register' element={ <Register />} />
        <Route path='/login' element={ <Login/> } />
        {/* <Route path='/carty' element={ <CartPage /> }/> */}
        <Route path='/cart' element={ getSessionData().token ? <CartPage /> : <Navigate to="/login" /> } />
        <Route path='/dashboard' element={<ProtectedRoute><DashboardPage /></ProtectedRoute> }/>


      </Routes>
    </>
  )
}
