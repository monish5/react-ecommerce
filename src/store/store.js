import cartReducer from "./cartSlice";
import productReducer from "./productSlice";

const { configureStore } = require("@reduxjs/toolkit");

export default configureStore({
    reducer:{
        products:productReducer,
        cart: cartReducer,
    }
});