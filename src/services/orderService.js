/* eslint-disable no-throw-literal */
import { getSessionData } from "./authService"

export async function createOrder(cartList, totalAmount, user) {
  const browserData = getSessionData();
  const order = {
    cartList,
    totalAmount,
    user: {
      id: user.id,
      name: user.name,
      email: user.email
    },
    UserId:user.id,
    numOfItems: cartList.length,
    amountPaid: totalAmount
  };

  const res = await fetch(`${process.env.REACT_APP_API_URL}660/orders`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${browserData.token}`,
    },
    body: JSON.stringify(order)
  });

  if(!res.ok) {
    throw { message: res.statusText, status: res.status};
  }
  const data = await res.json();
  return data;
}

export async function getOrders() {
  const browserData = getSessionData(); 
  console.log(`${process.env.REACT_APP_API_URL}orders?user.id=${browserData.userId}`);
  const res = await fetch(`${process.env.REACT_APP_API_URL}orders?user.id=${browserData.userId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${browserData.token}`,
    },
  });

  if(!res.ok) {
    throw { message: res.statusText, status: res.status};
  }

  const data = await res.json();
  return data;
}
