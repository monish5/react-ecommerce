import React from "react";
import { Link } from "react-router-dom";

export const DropdownLoggedOut = ({setIsDropdownVisible}) => {
  return (
    <div
      className="z-50 absolute top-10 right-0 my-4 text-base list-none bg-white divide-y divide-gray-100 rounded-lg shadow dark:bg-gray-700 dark:divide-gray-600"
      id="user-dropdown"
    >
      <div className="px-4 py-3">
        <span className="block text-sm text-gray-900 dark:text-white">
          Guest
        </span>
        <span className="block text-sm  text-gray-500 truncate dark:text-gray-400">
          
        </span>
      </div>
      <ul className="py-2" aria-labelledby="user-menu-button">
        <li>
          <Link
            onClick={() => setIsDropdownVisible(false) }
            to="/login"
            className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
          >
            Log In
          </Link>
        </li>
        <li>
          <Link
            onClick={() => setIsDropdownVisible(false) }
            to="/register"
            className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
          >
            Register
          </Link>
        </li>
      </ul>
    </div>
  );
};
